import React, { Component } from 'react';
import Modal from "./components/Modal/Modal";
import ModalButton from "./components/Modal/ModalButton/ModalButton";
import Alert from "./components/Alert/Alert";

import './App.css';

class App extends Component {
  state = {
    modalShow: false,
    colors: ['white', 'green', 'red', 'yellow'],
    alerts: [
      {type: 'primary', alertShow: false},
      {type: 'success', alertShow: false},
      {type: 'danger', alertShow: false},
      {type: 'warning', alertShow: false},
    ]
  };

  openModal = event => {
    event.preventDefault();
    this.setState({modalShow: true});
  };

  closeModal = event => {
    event.preventDefault();
    this.setState({modalShow: false});
  };

  openAlert = (event, index) => {
    event.preventDefault();

    const alerts = [...this.state.alerts];
    const alert = {...this.state.alerts[index]};
    alert.alertShow = true;
    alerts.splice(index, 1, alert);

    this.setState({alerts});
  };

  closeAlert = (event, index) => {
    event.preventDefault();

    const alerts = [...this.state.alerts];
    const alert = {...this.state.alerts[index]};
    alert.alertShow = false;
    alerts.splice(index, 1, alert);

    this.setState({alerts});
  };

  render() {
    return (
      <div className="App">
        <ModalButton btnClass="white" text="Open Modal" onClick={(event) => this.openModal(event)}/>
        <Modal
            show={this.state.modalShow}
            closed={(event) => (this.closeModal(event))}
            title="My modal"
        >
          <p>Some text here</p>
        </Modal>
        {this.state.colors.map((color, index) => (
            <ModalButton
                btnClass={color}
                text={"Open Alert " + color}
                onClick={(event) => (this.openAlert(event, index))}
                key={index}
            />
        ))}
        {this.state.alerts.map((alert, index) => (
            <Alert
                type={alert.type}
                dismiss={(event) => (this.closeAlert(event, index))}
                show={alert.alertShow}
                key={index}
            >
              This is a warning type alert
            </Alert>
        ))}
      </div>
    );
  }
}

export default App;
