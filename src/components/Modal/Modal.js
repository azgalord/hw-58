import React, {Fragment} from 'react';
import Backdrop from "./Backdrop/Backdrop";
import ModalCloseBtn from "./ModalCloseBtn/ModalCloseBtn";

import './Modal.css';

const Modal = props => {
  return (
      <Fragment>
        <Backdrop
            show={props.show}
        />
        <div
            className='Modal'
            style={{transform: props.show ? 'TranslateY(0)' : 'TranslateY(100vh)', opacity: props.show ? '1' : '0'}}
        >
          <h2 className="ModalTitle">{props.title}
            <ModalCloseBtn onClick={props.closed}/>
          </h2>
          {props.children}
        </div>
      </Fragment>

  );
};

export default Modal;
