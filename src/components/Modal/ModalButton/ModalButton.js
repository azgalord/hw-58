import React from 'react';
import './ModalButton.css';

const ModalButton = props => {
  return (
      <button
          onClick={props.onClick}
          className={props.btnClass}
      >
        {props.text}
      </button>
  );
};

export default ModalButton;
