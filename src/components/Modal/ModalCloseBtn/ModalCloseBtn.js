import React from 'react';
import Cross from "../../../assets/img/times-solid.svg";

import './ModalCloseBtn.css';

const ModalCloseBtn = props => {
  return (
      <button
          className="ModalCloseBtn"
          onClick={props.onClick}
          style={props.style}
      >
        <img src={Cross} alt=""/>
      </button>
  );
};

export default ModalCloseBtn;
