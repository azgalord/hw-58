import React, {Fragment} from 'react';
import ModalCloseBtn from "../Modal/ModalCloseBtn/ModalCloseBtn";
import Backdrop from "../Modal/Backdrop/Backdrop";

import './Alert.css';

const Alert = props => {
  return (
      <Fragment>
        <Backdrop show={props.show}/>
        <div className={props.type} style={{display: props.show ? 'flex' : 'none'}}>
          {props.children}
          <ModalCloseBtn
              onClick={props.dismiss}
              style={{opacity: props.dismiss !== undefined ? 1 : 0}}
          />
        </div>
      </Fragment>
  );
};

export default Alert;
